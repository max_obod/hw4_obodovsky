# Есть строка произвольного содержания. Написать код, который найдет в строке самое короткое слово,
# в котором присутствуют подряд две гласные буквы.

line = input("Enter the Line: ")
line_split = line.split()
line_split.sort(key=len, reverse=True)
print(line_split)

vowel_list = ['a', 'e', 'i', 'o', 'u', 'y', 'A', 'E', 'I', 'O', 'U', 'Y']

short_word = ""
for word in line_split:
    counter = 0
    for letter in word:
        if letter in vowel_list:
            counter += 1
        if counter == 2:
            short_word = word
            break
        if letter not in vowel_list:
            counter = 0

print(f"The shortest word with couple vowels: {short_word}")


# Есть два числа - минимальная цена и максимальная цена. Дан словарь продавцов и цен на какой то товар у разных продавцов:
# { "citrus": 47.999, "istudio" 42.999, "moyo": 49.999, "royal-service": 37.245, "buy.ua": 38.324, "g-store": 37.166, "ipartner": 38.988,
# "sota": 37.720, "rozetka": 38.003}. Написать код, который найдет и выведет на экран список продавцов,
# чьи цены попадают в диапазон между нижней и верхней ценой.

def function(arg):
 return lower_limit <= stores[store] <= upper_limit

stores = {
    "citrus": 47.999,
    "istudio": 42.999,
    "moyo": 49.999,
    "royal-service": 37.245,
    "buy.ua": 38.324,
    "g-store": 37.166,
    "ipartner": 38.988,
    "sota": 37.720,
    "rozetka": 38.003
}
lower_limit = 35.9
upper_limit = 37.3

for store in stores:
    if function(store) == True:
        print("Match:", store)
